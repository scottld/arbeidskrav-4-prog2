package edu.ntnu.stud.cardgame;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;
import java.util.List;

public class DeckOfCards {
  private final char[] suit = {'S', 'H', 'D', 'C'};
  private final List<PlayingCard> deck;
  Random random = new Random();

  public DeckOfCards() {
    deck = new ArrayList<>();

    for (int i = 0; i < 52; i++) {
      char s = suit[i / 13];
      int f = (i % 13) + 1;
      deck.add(new PlayingCard(s, f));
    }
  }

  public List<PlayingCard> getDeck() {
    return deck;
  }

  public HandOfCards dealHand(int n) {
    if (n < 0 || n > deck.size()) {
      throw new IllegalArgumentException("Invalid number of cards to deal");
    }
    HandOfCards hand = new HandOfCards();
    for (int i = 0; i < n; i++) {
      int cardIndex = random.nextInt(deck.size());
      if (hand.getHand().contains(deck.get(cardIndex))) {
        i--;
      } else {
        hand.addCard(deck.get(cardIndex));
      }
    }
    return hand;
    }

    @Override
    public String toString () {
      StringBuilder sb = new StringBuilder();
      for (PlayingCard card : deck) {
        sb.append(card.getAsString());
        sb.append("\n");
      }
      return sb.toString();
    }
  }
