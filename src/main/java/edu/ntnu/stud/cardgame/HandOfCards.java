package edu.ntnu.stud.cardgame;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class HandOfCards {
  private final List<PlayingCard> hand;

  public HandOfCards() {
    hand = new ArrayList<>();
  }

  public List<PlayingCard> getHand() {
    return hand;
  }

  public void addCard(PlayingCard card) {
    hand.add(card);
  }

  public int getSum() {
    return hand.stream()
        .mapToInt(PlayingCard::getFace)
        .reduce(0, Integer::sum);
  }

  public String findHearts() {
    String hearts = hand.stream()
        .map(PlayingCard::getAsString)
        .filter(s -> s.charAt(0) == 'H')
        .collect(Collectors.joining(" "));

    if (hearts.isEmpty()) {
      return "No hearts";
    }
    return hearts.trim();
  }

  public boolean containsQueenOfSpades() {
    return hand.stream()
        .map(PlayingCard::getAsString)
        .anyMatch(s -> s.equals("S12"));
  }

  public boolean contains5Flush() {
    HashMap<String, Integer> numberOfColors = new HashMap<>();
    numberOfColors.put("Red", 0);
    numberOfColors.put("Black", 0);

    hand.stream()
        .map(PlayingCard::getSuit)
        .forEach(character -> {
          if (character == 'H' || character == 'D') {
            int num = numberOfColors.get("Red");
            numberOfColors.put("Red", num+1);
          } else if (character == 'S' || character == 'C') {
            int num = numberOfColors.get("Black");
            numberOfColors.put("Black", num+1);
          }
        });

    return numberOfColors.get("Red") >= 5 || numberOfColors.get("Black") >= 5;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();

    hand.forEach(card -> sb.append(card.getAsString()).append(", "));

    return sb.toString();
  }


}
