package edu.ntnu.stud.cardgame;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Main extends Application {
  DeckOfCards deck = new DeckOfCards();
  HandOfCards hand = new HandOfCards();
  Text sumText = new Text();
  Text heartsText = new Text();
  Text queenSpadesText = new Text();
  Text fiveFlushText = new Text();
  Text cardsDealt = new Text();
  @Override
  public void start(Stage stage) throws Exception {
    stage.setTitle("Card game");

    VBox root = new VBox();
    root.setSpacing(20);
    root.setAlignment(Pos.CENTER);
    HBox topHalf = new HBox();
    topHalf.setSpacing(20);
    StackPane cardShowingArea = new StackPane();
    Rectangle border = new Rectangle(200, 200);
    border.setFill(Color.TRANSPARENT);
    border.setStroke(Color.BLACK);
    cardShowingArea.getChildren().add(border);

    VBox buttons = new VBox();
    buttons.setSpacing(10);
    Button dealButton = new Button("Deal");
    dealButton.setOnAction(e -> dealEvent());
    Button checkButton = new Button("Check");
    checkButton.setOnAction(e -> checkEvent());
    cardShowingArea.getChildren().add(cardsDealt);

    buttons.getChildren().add(dealButton);
    buttons.getChildren().add(checkButton);

    topHalf.getChildren().add(cardShowingArea);
    topHalf.getChildren().add(buttons);
    topHalf.setAlignment(Pos.CENTER);

    TilePane bottomHalf = new TilePane();
    bottomHalf.getChildren().add(sumText);
    bottomHalf.getChildren().add(heartsText);
    bottomHalf.getChildren().add(queenSpadesText);
    bottomHalf.getChildren().add(fiveFlushText);
    HBox container = new HBox();
    container.getChildren().add(bottomHalf);
    container.setAlignment(Pos.CENTER);

    root.getChildren().add(topHalf);
    root.getChildren().add(container);
    root.setFillWidth(true);
    stage.setScene(new Scene(root));
    stage.setMaximized(true);
    stage.show();
  }

  public void dealEvent() {
    hand = deck.dealHand(5);
    cardsDealt.setText(hand.toString());
  }

  public void checkEvent() {
    sumText.setText("Sum: " + hand.getSum());
    heartsText.setText("Hearts: " + hand.findHearts());
    queenSpadesText.setText("Queen of spades: " + hand.containsQueenOfSpades());
    fiveFlushText.setText("Five flush: " + hand.contains5Flush());
  }

  public static void main(String[] args) {
    launch(args);
  }
}
