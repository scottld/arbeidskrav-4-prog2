module com.example.cardgame {
  requires javafx.controls;
  requires javafx.fxml;


  opens edu.ntnu.stud.cardgame to javafx.fxml;
  exports edu.ntnu.stud.cardgame;
}