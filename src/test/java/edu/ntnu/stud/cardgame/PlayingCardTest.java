package edu.ntnu.stud.cardgame;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PlayingCardTest {
  PlayingCard card1;
  PlayingCard card2;
  @BeforeEach
  public void setUp() {
    card1 = new PlayingCard('H', 3);
  }

  @Nested
  public class PositiveTests {
    @Test
    void testGetAsString() {
      assertEquals("H3", card1.getAsString());
    }
  }

  @Nested
  public class NegativeTests {
    @Test
    void testConstructor() {
      assertThrows(IllegalArgumentException.class, () -> card2 = new PlayingCard('E', 10));
      assertThrows(IllegalArgumentException.class, () -> card2 = new PlayingCard('D', 90));
      assertThrows(IllegalArgumentException.class, () -> card2 = new PlayingCard('H', 0));
    }
  }
}
