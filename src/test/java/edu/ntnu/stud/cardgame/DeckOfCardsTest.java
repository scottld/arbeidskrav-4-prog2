package edu.ntnu.stud.cardgame;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class DeckOfCardsTest {
  DeckOfCards deck;

  @BeforeEach
  void setDeck() {
    deck = new DeckOfCards();
  }

  @Nested
  public class PositiveTests{
    @Test
    void testDeckFiller(){
      assertEquals(deck.getDeck().size(), 52);
      assertTrue(deck.getDeck().contains(new PlayingCard('H', 10)));
      assertTrue(deck.getDeck().contains(new PlayingCard('D', 1)));
      assertTrue(deck.getDeck().contains(new PlayingCard('C', 5)));
    }
  }
}
