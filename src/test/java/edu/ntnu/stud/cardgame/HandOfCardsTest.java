package edu.ntnu.stud.cardgame;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class HandOfCardsTest {
  HandOfCards hand;
  PlayingCard card1;
  PlayingCard card2;
  PlayingCard card3;
  PlayingCard card4;

  @BeforeEach
  void setHand() {
    hand = new HandOfCards();
    card1 = new PlayingCard('H', 12);
    card2 = new PlayingCard('D', 2);
    card3 = new PlayingCard('C', 7);
    card4 = new PlayingCard('S', 12);
  }

  @Nested
  class PositiveTests {
    @Test
    void testAddCard() {
      hand.addCard(card1);
      assertTrue(hand.getHand().contains(card1));
    }

    @Test
    void testGetSum() {
      hand.addCard(card1);
      hand.addCard(card2);
      hand.addCard(card3);
      assertEquals(hand.getSum(),21);
    }

    @Test
    void testFindHearts() {
      hand.addCard(card1);
      assertEquals(hand.findHearts(), card1.getAsString());
    }

    @Test
    void testContainsQueenOfSpades(){
      assertFalse(hand.containsQueenOfSpades());
      hand.addCard(card4);
      assertTrue(hand.containsQueenOfSpades());
    }

    @Test
    void testContains5Flush() {
      assertFalse(hand.contains5Flush());

      for (int i = 0; i<5; i++){
        hand.addCard(card2);
      }

      assertTrue(hand.contains5Flush());
    }
  }
}
